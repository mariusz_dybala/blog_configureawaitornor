using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;

namespace ConfigureAwaitOrNor;

public partial class MainPageViewModel : ObservableObject
{
    private const string ContentUrl = "https://www.google.com/";
    
    [ObservableProperty]
    private string _messageUpdatedByBindings;
    [ObservableProperty]
    private string _messageWithWinner;

    public Action<string> VeryBadDelegateWithUpdateUI { get; set; }

    [RelayCommand]
    private async Task OpenInBrowser()
    {
        Console.WriteLine($"OpenInBrowser - Before async call: {Thread.CurrentThread.ManagedThreadId}");
        await GetContentAsync().ConfigureAwait(false);

        Console.WriteLine($"OpenInBrowser - After async call: {Thread.CurrentThread.ManagedThreadId}");

        await Browser.OpenAsync(ContentUrl);
    }

    [RelayCommand]
    private async Task UpdateMessageDirectly()
    {
        Console.WriteLine($"UpdateMessageDirectly - Before async call: {Thread.CurrentThread.ManagedThreadId}");
        await GetContentAsync().ConfigureAwait(false);

        Console.WriteLine($"UpdateMessageDirectly - After async call: {Thread.CurrentThread.ManagedThreadId}");

        VeryBadDelegateWithUpdateUI?.Invoke("You have been updated directly");
    }

    [RelayCommand]
    private async Task UpdateMessageByBinding()
    {
        Console.WriteLine($"UpdateMessageByBinding - Before async call: {Thread.CurrentThread.ManagedThreadId}");
        await GetContentAsync().ConfigureAwait(false);

        Console.WriteLine($"UpdateMessageByBinding - After async call: {Thread.CurrentThread.ManagedThreadId}");

        MessageUpdatedByBindings = "You have been updated by binding";
    }

    [RelayCommand]
    private async Task GetTheWinnerThread()
    {
        Console.WriteLine($"GetTheWinner - Before async call: {Thread.CurrentThread.ManagedThreadId}");
        await GetContentAsync().ConfigureAwait(false);

        Console.WriteLine($"GetTheWinner - After async call: {Thread.CurrentThread.ManagedThreadId}");

        MainThread.InvokeOnMainThreadAsync(UpdateWinnerMessageFromMainThread);

        Console.WriteLine($"GetTheWinner - From random thread execution: {Thread.CurrentThread.ManagedThreadId}");
        MessageWithWinner = "Some random thread LOST";
    }

    private void UpdateWinnerMessageFromMainThread()
    {
        Console.WriteLine($"GetTheWinner - From main thread execution:  {Thread.CurrentThread.ManagedThreadId}");
        MessageWithWinner = "Main thread LOST";
    }

    private async Task GetContentAsync()
    {
        var client = new HttpClient();
        _ = await client.GetStringAsync(ContentUrl);
    }

    // ConfigureAwait(true)
    // Button click
    // public async Task OnDownloadContentCommand()
    // {
    //     var client = new HttpClient();
    //     // Http request
    //     _ = await client.GetStringAsync(ContentUrl).ConfigureAwait(true); // .ConfigureAwait(true) is default
    //     // Button click continuation
    // }

    // ConfigureAwait(false)
    // Button click
    public async Task OnDownloadContentCommand()
    {
        var client = new HttpClient();
        // Http request
        _ = await client.GetStringAsync(ContentUrl).ConfigureAwait(false);
        // Button click continuation
    }
}