﻿using System.ComponentModel;

namespace ConfigureAwaitOrNor;

public partial class MainPage : ContentPage
{
    int count = 0;

    public MainPage(MainPageViewModel vm)
    {
        InitializeComponent();

        BindingContext = vm;
        
        vm.VeryBadDelegateWithUpdateUI = VeryBadDelegateWithUpdateUi;
        
        UpdatedByBindingLabel.PropertyChanged += UpdatedByBindingLabelOnPropertyChanged;
    }

    private void UpdatedByBindingLabelOnPropertyChanged(object? sender, PropertyChangedEventArgs e)
    {
        if (e.PropertyName == nameof(Label.Text))
        {
            Console.WriteLine($"UpdateMessageByBinding - View update attempt: {Thread.CurrentThread.ManagedThreadId}");
        }
    }


    private void VeryBadDelegateWithUpdateUi(string message)
    {
        Console.WriteLine($"UpdateMessageDirectly - View update attempt: {Thread.CurrentThread.ManagedThreadId}");
        UpdatedDirectlyLabel.Text = message;
    }
}